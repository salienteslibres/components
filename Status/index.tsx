// eslint-disable-next-line no-use-before-define
import React, { FC } from 'react'
import { makeStyles, Typography } from '@material-ui/core'
import Dot, { DotVariants } from 'components/Dot'

export type ParseStatusKey = 'closed' | 'draft' | 'filed' | 'published'

export const ParseStatus: Record<ParseStatusKey, {
  dot: DotVariants;
  label: string
}> = {
  closed: {
    dot  : 'error',
    label: 'Cerrado'
  },
  draft: {
    dot  : 'default',
    label: 'Borrador'
  },
  filed: {
    dot  : 'default',
    label: 'Archivado'
  },
  published: {
    dot  : 'success',
    label: 'Publicado'
  }
}

interface StatusProps {
  status: ParseStatusKey;
}

const Status: FC<StatusProps> = ({ status }) => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Dot variant={ParseStatus[status].dot} />
      <Typography className={classes.marginLeft} variant='body1'>{ParseStatus[status].label}</Typography>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  marginLeft: {
    marginLeft: theme.spacing(1)
  },
  root: {
    alignItems: 'center',
    display   : 'flex'
  }
}), { name: 'Status' })

export  default Status
