// eslint-disable-next-line no-use-before-define
import React, { FC } from 'react'
import { IconButton, makeStyles, Paper } from '@material-ui/core'
import { Business as BusinessIcon, Close as CloseIcon } from '@material-ui/icons'
import clsx from 'clsx'

interface MiniPhotoProps {
  className?: string;
  photo: string;
  onClose?: () => void;
  main?: boolean;
  select?: boolean;
  onClick?: () => void;
}

const MiniPhoto: FC<MiniPhotoProps> = ({ className, onClose, main, photo, select, onClick }) => {
  const classes = useStyles()

  return (
    <Paper
      className={clsx(classes.root, className, {
        [classes.select]: select
      })}
      onClick={onClick} variant='outlined'>
      <img
        alt='imagen'
        className={classes.img}
        src={photo} />
      {onClose && (
        <IconButton
          className={classes.closeIconButton}
          onClick={onClose}
          size='small'>
          <CloseIcon fontSize='small' />
        </IconButton>
      )}
      {main && (
        <IconButton
          className={classes.businessIcon}
          size='small'>
          <BusinessIcon fontSize='small' />
        </IconButton>
      )}
    </Paper>
  )
}

const useStyles = makeStyles((theme) => ({
  businessIcon: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main
    },
    backgroundColor: theme.palette.secondary.main,
    borderRadius   : `${theme.shape.borderRadius}px 0 0 0`,
    bottom         : 0,
    color          : 'white',
    position       : 'absolute',
    right          : 0
  },
  closeIconButton: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main
    },
    backgroundColor: theme.palette.secondary.main,
    color          : 'white',
    marginRight    : -theme.spacing(11 / 8),
    marginTop      : -theme.spacing(11 / 8),
    padding        : 1,
    position       : 'absolute',
    right          : 0,
    top            : 0
  },
  img: {
    display  : 'block',
    height   : '100%',
    objectFit: 'cover',
    width    : '100%'
  },
  root: {
    '&:hover': {
      border: `1px solid ${theme.palette.primary.main}`
    },
    height  : 120,
    minWidth: 86,
    position: 'relative',
    width   : 86
  },
  select: {
    '& $businessIcon': {
      '&:hover': {
        backgroundColor: theme.palette.primary.main
      },
      backgroundColor: theme.palette.primary.main
    },
    '& $closeIconButton': {
      '&:hover': {
        backgroundColor: theme.palette.primary.main
      },
      backgroundColor: theme.palette.primary.main
    },
    border: `1px solid ${theme.palette.primary.main}`
  }
}), { name: 'MiniPhoto' })

export default MiniPhoto
