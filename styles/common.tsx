import { makeStyles } from '@material-ui/core'

const useCommonStyles = makeStyles((theme) => ({
  expandHeightContainer: {
    display      : 'flex',
    flexDirection: 'column'
  },
  expandHeightTarget: {
    flex     : 1,
    minHeight: 0
  },
  flex: {
    alignItems: 'center',
    display   : 'flex'
  },
  mg: {
    margin: theme.spacing(1.5)
  },
  mgL: {
    margin: theme.spacing(2.5)
  },
  mgS: {
    margin: theme.spacing(1)
  },
  mgXS: {
    margin: theme.spacing(.5)
  },
  mgl: {
    marginLeft: theme.spacing(1.5)
  },
  mglL: {
    marginLeft: theme.spacing(2.5)
  },
  mglS: {
    marginLeft: theme.spacing(1)
  },
  mglXS: {
    marginLeft: theme.spacing(.5)
  },
  mgr: {
    marginRight: theme.spacing(1.5)
  },
  mgrL: {
    marginRight: theme.spacing(2.5)
  },
  mgrS: {
    marginRight: theme.spacing(1)
  },
  mgrXS: {
    marginRight: theme.spacing(.5)
  },
  pd: {
    padding: theme.spacing(1.5)
  },
  pdL: {
    padding: theme.spacing(2.5)
  },
  pdS: {
    padding: theme.spacing(1)
  },
  pdXS: {
    padding: theme.spacing(.5)
  },
  pdl: {
    paddingLeft: theme.spacing(1.5)
  },
  pdlL: {
    paddingLeft: theme.spacing(2.5)
  },
  pdlS: {
    paddingLeft: theme.spacing(1)
  },
  pdlXS: {
    paddingLeft: theme.spacing(.5)
  },
  pdr: {
    paddingRight: theme.spacing(1.5)
  },
  pdrL: {
    paddingRight: theme.spacing(2.5)
  },
  pdrS: {
    paddingRight: theme.spacing(1)
  },
  pdrXS: {
    paddingRight: theme.spacing(.5)
  },
  spaceAround: {
    justifyContent: 'space-around',
    width         : '100%'
  },
  spaceBetween: {
    justifyContent: 'space-between',
    width         : '100%'
  }
}), { name: 'useCommonStyles' })

export default useCommonStyles
