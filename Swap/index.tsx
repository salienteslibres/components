import React, { FC } from 'react'
import { makeStyles } from '@material-ui/core'
import clsx from 'clsx'

export interface SwapProps {
  hoverComponent: React.ReactNode;
  children: React.ReactNode;
  active?: boolean;
  className?: string;
}

const Swap: FC<SwapProps> = ({ hoverComponent, children, active, className }) => {
  const classes = useStyles()

  if(active !== undefined)

    if(active)
      return (
        <div>
          {hoverComponent}
        </div>
      )
    else
      return (
        <div>
          {children}
        </div>
      )

  return (
    <div className={clsx(className, classes.root)}>
      <div className={classes.hover}>
        {hoverComponent}
      </div>
      <div className={classes.default}>
        {children}
      </div>
    </div>
  )
}

const useStyles = makeStyles({
  'default': { display: 'block' },
  hover    : { display: 'none' },
  root     : {
    '&:hover': {
      '& $default': {
        display: 'none'
      },
      '& $hover': {
        display: 'block'
      }
    }
  }
}, { name: 'Swap' })

export default Swap
