import { grey } from '@material-ui/core/colors'
import {
  createMuiTheme,
  SimplePaletteColorOptions
} from '@material-ui/core/styles'
import { ColorPartial } from '@material-ui/core/styles/createPalette'

// http://mcg.mbitson.com/#!?mcgpalette0=%23ff4ab6

export type PaletteColor = SimplePaletteColorOptions & ColorPartial

const primaryColor = {
  100         : '#FFC9E9',
  200         : '#FFA5DB',
  300         : '#FF80CC',
  400         : '#FF65C1',
  50          : '#FFE9F6',
  500         : '#FF4AB6',
  600         : '#FF43AF',
  700         : '#FF3AA6',
  800         : '#FF329E',
  900         : '#FF228E',
  A100        : '#FFFFFF',
  A200        : '#FFFFFF',
  A400        : '#FFD1E6',
  A700        : '#FFB8D9',
  contrastText: '#ffffff',
  dark        : '#FF329E',
  light       : '#FFA5DB',
  main        : '#FF4AB6'
}

const secondaryColor = {
  100                 : '#ecd4f8',
  200                 : '#e0b7f4',
  300                 : '#d49af0',
  400                 : '#ca84ec',
  50                  : '#f8eefc',
  500                 : '#c16ee9',
  600                 : '#bb66e6',
  700                 : '#b35be3',
  800                 : '#ab51df',
  900                 : '#9e3fd9',
  A100                : '#ffffff',
  A200                : '#fbf6ff',
  A400                : '#e7c3ff',
  A700                : '#dda9ff',
  contrastDefaultColor: '#ffffff',
  contrastText        : '#ffffff',
  dark                : '#ab51df', // 800
  light               : '#e0b7f4', // 200
  main                : '#c16ee9'  // 500
}
// Create a theme instance.
export const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        minWidth     : 0,
        padding      : '4px 8px',
        textTransform: 'initial'
      }
    },
    MuiCssBaseline: {
      '@global': {
        '*::-webkit-scrollbar': {
          width: '4px'
        },
        '*::-webkit-scrollbar-thumb': {
          backgroundColor: secondaryColor[300],
          borderRadius   : 4
        },
        '*::-webkit-scrollbar-track': {
          // '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)'
        }
      }
    },
    MuiInputBase: {
      input: {
        '&:-webkit-autofill': {
          transitionDelay   : '9999s',
          transitionProperty: 'background-color, color'
        }
      }
    },
    MuiListItem: {
      button: {
        '&:hover': {
          backgroundColor: primaryColor[50]
        }
      },
      root: {
        '&$focusVisible': {
          '&:hover': {
            backgroundColor: primaryColor[50]
          },
          backgroundColor: primaryColor[50]
        },
        '&$selected': {
          '&:hover': {
            backgroundColor: primaryColor[50]
          },
          backgroundColor: primaryColor[50]
        }
      }
    },
    MuiMenuItem: {
      root: {
        minHeight: 36
      }
    },
    MuiOutlinedInput: {
      inputMarginDense: {
        paddingBottom: 8,
        paddingTop   : 8
      },
      notchedOutline: {
        borderColor: primaryColor.main
      }
    },
    MuiSelect: {
      iconOutlined: {
        color: primaryColor.main
      },
      outlined: {
        color: primaryColor.main
      },
      root: {
        backgroundColor: 'white',
        padding        : 8
      }
    }
  },
  palette: {
    primary  : primaryColor,
    secondary: secondaryColor
  },
  typography: {
    body1: {
      fontSize  : 12,
      fontWeight: 'lighter'
    },
    body2: {
      fontSize   : 14,
      wordSpacing: 2
    },
    h5: {
      fontSize  : 18,
      fontWeight: 'bold'
    },
    h6: {
      fontSize: 14
    },
    subtitle2: {
      color        : grey[800],
      fontSize     : 14,
      textTransform: 'uppercase'
    }
  }
})

export default theme
