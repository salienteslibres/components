// eslint-disable-next-line no-use-before-define
import React from 'react'
import { ChangeEvent, FC, ReactNode } from 'react'
import { Button, ButtonProps, makeStyles } from '@material-ui/core'
import clsx from 'clsx'

type Upload = ButtonProps & {
  label?: string;
  icon?: ReactNode;
  // eslint-disable-next-line no-unused-vars
  onSelectFile?: (file: File) => void
}

const Upload: FC<Upload> = ({ icon, label = 'Subir', onSelectFile, ...props }) => {
  const classes = useStyles()

  const _handleChange = ({ target: { files } }: ChangeEvent<HTMLInputElement>) => {
    if(files && onSelectFile)
      onSelectFile(files[0])
  }

  return (
    <div>
      <input
        accept='audio/*,video/*,image/*'
        className={classes.input}
        id='raised-button-file'
        multiple
        onChange={_handleChange}
        type='file' />
      <label className={classes.label} htmlFor='raised-button-file'>
        <Button
          component='span' /* variant='raised' */
          {...props}
          className={clsx(classes.button, props.className)}>
          {icon || label}
        </Button>
      </label>
    </div>
  )
}

const useStyles = makeStyles(() => ({
  button: {
    display: 'flex',
    height : '100%'
  },
  input: {
    display: 'none'
  },
  label: {
    display: 'block',
    height : '100%'
  }
}), { name: 'Upload' })

export default Upload
