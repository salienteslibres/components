// eslint-disable-next-line no-use-before-define
import React, { FC } from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core'
import { blue, green, grey, red } from '@material-ui/core/colors'

export type DotVariants = 'default' | 'info' | 'success' | 'disabled' | 'error'

export interface DotProps {
  variant?: DotVariants
}

const Dot: FC<DotProps> = ({ variant = 'default' }) => {
  const classes = useStyles()

  return (
    <div className={clsx(classes.root, classes[variant])} />
  )
}

const useStyles = makeStyles(() => ({
  'default': {
    backgroundColor: grey[300]
  },
  disabled: {
    backgroundColor: grey[300]
  },
  error: {
    backgroundColor: red[500]
  },
  info: {
    backgroundColor: blue[100]
  },
  root: {
    borderRadius: '50%',
    height      : 8,
    width       : 8
  },
  success: {
    backgroundColor: green[500]
  }
}), { name: 'Dot' })

export default Dot
