import { withStyles } from '@material-ui/core'
import { Rating as MuiRating } from '@material-ui/lab'
import { PaletteColor } from 'components/theme'

const Rating = withStyles((theme) => ({
  iconFilled: {
    color: (theme.palette.primary as PaletteColor)[500]
  },
  iconHover: {
    color: (theme.palette.primary as PaletteColor)[800]
  }
}))(MuiRating)

export default Rating
