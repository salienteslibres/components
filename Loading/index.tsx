import { makeStyles } from '@material-ui/core'
import React from 'react'

export default () => {
  const classes = useStyles()

  return (
    <div
      className={classes.root}>
      <img alt='loading' src='https://cdn.krowdy.com/images/loader.gif' />
    </div>
  )
}

const useStyles = makeStyles(() => ({
  root: {
    alignItems    : 'center',
    bottom        : 0,
    display       : 'flex',
    justifyContent: 'center',
    left          : 0,
    position      : 'absolute',
    right         : 0,
    top           : 0
  }
}), { name: 'Loading' })
